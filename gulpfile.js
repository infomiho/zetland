var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var connect = require('gulp-connect');

var paths = {
	js: ['src/scripts/vendor/*.js', 'src/scripts/app.js'],
	css: ['src/styles/main.less'],
	html: ['index.html', 'pricelist.html'],
	watch: {
		js: ['src/scripts/**/*.js'],
		css: ['src/styles/**/*.less'],
		html: ['index.html', 'pricelist.html']
	}
}

gulp.task('less', function () {
	return gulp.src(paths.css)
	.pipe(less({compress: true}))
	.on('error', function(err){ console.log(err.message); })
	.pipe(gulp.dest('build'))
	.pipe(connect.reload());
});	
gulp.task('scripts', function () {
	return gulp.src(paths.js)
	.pipe(concat('all.js'))
	.pipe(uglify())
	.pipe(gulp.dest('build'))
	.pipe(connect.reload());
});
gulp.task('html', function() {
	return gulp.src(paths.html)
	.pipe(connect.reload());
});
gulp.task('connect', ['less', 'scripts'], function() {
  return connect.server({livereload: true});
});
gulp.task('default', ['less', 'scripts', 'connect'], function () {
	gulp.watch(paths.watch.css, ['less']);
	gulp.watch(paths.watch.js, ['scripts']);
	gulp.watch(paths.watch.html, ['html'])
});