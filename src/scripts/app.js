(function () {
	var app = {};
	/* Copyright @infomiho */
	app.equalized = false;
	app.equalize = function (context) {
		var candidates = context ? context.find(".equalize") : $(".equalize-parent .equalize");
		//min-width of the md collapse
		if ($(window).width() > 991) {
			var max = 0,			
			first = 0,
			changed = false;
			candidates.each(function (index, item) {
				if (app.equalized) $(item).height("auto");
				//save the first value
				first = max == 0 ? $(item).height() : first;
				max = Math.max($(item).height(), max);	
				//any of the values if different from the first	
				if (max != first) { 
					first = max; 
					changed = true;
				}	
			});
			if (changed) {
				app.equalized = true;
				candidates.each(function (index, item) {
					$(item).height(max);
				});
			}
		} else {
			if (app.equalized) {
				candidates.each(function (index, item) {
					$(item).height("auto");
				});
			}
		}
	};
	app.accordian = function () {
		$(".accordian .title").on("click", function () {
			var item = $(this).closest(".item");
			var hadClass = item.hasClass("active");			
			$(this).closest(".item").toggleClass("active");
			if(!hadClass) {
				app.equalize(item);
			}
		});
	};
	//thanks http://sampsonblog.com/749/simple-throttle-function
	app.throttle = function(callback, limit) {
	    var wait = false;                 // Initially, we're not waiting
	    return function () {              // We return a throttled function
	        if (!wait) {                  // If we're not waiting
	            callback.call();          // Execute users function
	            wait = true;              // Prevent future invocations
	            setTimeout(function () {  // After a period of time
	                wait = false;         // And allow future invocations
	            }, limit);
	        }
	    }
	}
	app.init = function () {
		$(function () {
			app.equalize();
			app.accordian();
			$('.expanded-map .hover').popover();
		});
		//equalize on window resize, for performace we use throttle
		var throttled = app.throttle(function () {
			app.equalize();
		}, 150);
		$(window).resize(throttled);
		/*$(".expanded-map").on("click", function (e) {
			var parentOffset = $(this).offset(); 		
		   var relX = e.pageX - parentOffset.left - 20;
		   var relY = e.pageY - parentOffset.top -2;
		   alert(relY + ' '+ relX);
		});*/
	};
	app.init();
})();